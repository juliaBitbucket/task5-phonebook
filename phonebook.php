<?php
    $contacts = json_decode(file_get_contents(__DIR__ . '/phone_book.json'), true);

    if(empty($contacts)) {
        echo 'Data not found.';
        exit;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Phone book</title>
    <meta charset="utf-8">
</head>
    <body>
        <table border="1">
            <caption>Phone Book</caption>
            <tr>
                <th>Number</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Phone</th>
            </tr>
            <?php foreach ($contacts as $key => $contact): ?>
            <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo (isset($contact['firstName']) ? $contact['firstName'] : 'N/a') ?></td>
                <td><?php echo (isset($contact['lastName']) ? $contact['lastName'] : 'N/a') ?></td>
                <td><?php echo (isset($contact['address']) ? $contact['address'] : 'N/a') ?></td>
                <td><?php echo (isset($contact['phoneNumber']) ? $contact['phoneNumber'] : 'N/a') ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>
